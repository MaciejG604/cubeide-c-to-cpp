**Helper program for using C++ in CubeIDE**

As of now CubeIDE does not support .cpp and .hpp files while updating hardware configuration.  
This program aims to automatically do all the clean-up related to duplicated main.c/cpp and main.h/hpp.

**Setup** - **2.** and **3.** have to be done for every new project  
1. Copy the file _CubeIDE_C_to_CPP.jar_ file to your CubeIDE home directory  
2. In CubeIDE rightclick on a project go to _Properties > C/C++ Build > Settings > Build Steps_  
3. Copy "java -jar ${eclipse_home}CubeIDE_C_to_CPP.jar ${ProjDirPath}" into _Pre-build steps > Command:_  



Now everytime you build your project the program will run and clean up the files if needed.

If you don't know your CubeIDE home directory just do **2.** and **3.** and observe the paths that apear in the console.

**Notice:**  

Before building your project again remove the built binaries by rightclicking the project and choosing _Clean Project_ for everything to compile error free.
