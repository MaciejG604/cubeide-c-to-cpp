package Main;

import FileMerger.FileMerger;
import FileMerger.FileIOError;
import FileMerger.InFileInstanceReplacer;

import java.awt.*;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

import static java.nio.file.FileVisitResult.CONTINUE;

public class Main {

    public static void main(String[] args) {
        if (args.length == 0)
        {
            System.err.println("No argument path provided. Exiting...");
            return;
        }

        Path corePath = null;
        try
        {
            corePath = Paths.get(args[0], "Core");
        }
        catch(InvalidPathException exp)
        {
            System.err.println("Invalid path argument. Exiting...");
            return;
        }

        Path incPath = corePath.resolve("Inc");
        Path srcPath = corePath.resolve("Src");

        Path mainCpp = srcPath.resolve("main.cpp");
        Path mainC = srcPath.resolve("main.c");

        boolean srcReplaceNeeded = false;

        if (Files.exists(mainCpp) && Files.exists(mainC))
        {
            if (mergeFiles(mainCpp, mainC))
                srcReplaceNeeded = true;
        }

        Path mainHpp = incPath.resolve("main.hpp");
        Path mainH = incPath.resolve("main.h");

        boolean incReplaceNeeded = false;

        if (Files.exists(mainHpp) && Files.exists(mainH))
        {
            if (mergeFiles(mainHpp, mainH))
                incReplaceNeeded = true;
        }

        SimpleFileVisitor<Path> replaceInstanceVisitor = new SimpleFileVisitor<>()
        {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException
            {

                InFileInstanceReplacer mainHReplacer = new InFileInstanceReplacer(file, "\"main.h\"", "\"main.hpp\"");
                mainHReplacer.replace();

                return CONTINUE;
            }

        };

        try
        {
            if (incReplaceNeeded)
            {
                Files.walkFileTree(srcPath, replaceInstanceVisitor);
                Files.walkFileTree(incPath, replaceInstanceVisitor);
            }
        }
        catch (java.io.IOException exc)
        {
            System.err.println(exc.getMessage());
        }
    }

    public static boolean mergeFiles(Path updateFilePath, Path mergeNDeleteFilePath)
    {
        try
        {
            FileMerger merger = new FileMerger(updateFilePath, mergeNDeleteFilePath, "USER CODE BEGIN", "USER CODE END");

            ArrayList<FileIOError> mergeErrors = merger.merge();

            if ( mergeErrors != null )
            {
                for (FileIOError ioError : mergeErrors)
                {
                    System.err.println("File corrupted in line: " + ioError.getLineNumber());
                    System.err.println("Reason: " + ioError.getMessage());
                }
                return false;
            }

            return true;
        }
        catch (java.io.IOException exc)
        {
            System.err.println(exc.getMessage());
            return false;
        }
    }
}
