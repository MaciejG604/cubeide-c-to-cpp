package FileMerger;

import java.nio.file.Path;

public class FileIOError
{
    private Path errorFilePath_;
    private int lineNumber_;
    private String message_;

    public FileIOError( Path errorFilePath, int lineNumber, String message )
    {
        this.errorFilePath_ = errorFilePath;
        this.lineNumber_ = lineNumber;
        this.message_ = message;
    }

    public Path getErrorFilePath()
    {
        return errorFilePath_;
    }

    public int getLineNumber()
    {
        return lineNumber_;
    }

    public String getMessage()
    {
        return message_;
    }
}
