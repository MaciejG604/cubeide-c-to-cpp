package FileMerger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileMerger
{
    private final Path toUpdateFilePath_, mergeNDeleteFilePath_;
    private final ArrayList<String> inputLines_ = new ArrayList<>();
    private final ArrayList<String> outputLines_= new ArrayList<>();
    private final Matcher openingMatcher_, closingMatcher_;
    private final String openingClause_, closingClause_;
    private final Map<String, List<String>> fragments_ = new TreeMap<>();

    public FileMerger( Path toUpdateFilePath, Path mergeNDeleteFilePath, String openingClause, String closingClause) throws IOException
    {
        inputLines_.addAll(Files.readAllLines(toUpdateFilePath));
        outputLines_.addAll(Files.readAllLines(mergeNDeleteFilePath));

        toUpdateFilePath_ = toUpdateFilePath;
        mergeNDeleteFilePath_ = mergeNDeleteFilePath;
        openingClause_ = openingClause;
        closingClause_ = closingClause;
        Pattern openingRegex = Pattern.compile(".*" + openingClause + ".*");
        Pattern closingRegex = Pattern.compile(".*" + closingClause + ".*");
        openingMatcher_ = openingRegex.matcher("");
        closingMatcher_ = closingRegex.matcher("");
    }

    public ArrayList<FileIOError> merge() throws IOException
    {
        backupFiles(toUpdateFilePath_, mergeNDeleteFilePath_);

        FileIOError readFileIOError = readOldFileFragments();
        FileIOError writeFileIOError = writeFileFragments();
        if ( readFileIOError == null && writeFileIOError == null)
        {
            Files.delete(mergeNDeleteFilePath_);
            Files.write(toUpdateFilePath_, outputLines_);
            return null;
        }

        ArrayList<FileIOError> fileIOErrorList = new ArrayList<>();

        if ( readFileIOError != null)
            fileIOErrorList.add(readFileIOError);
        if ( writeFileIOError != null)
            fileIOErrorList.add(writeFileIOError);

        return fileIOErrorList;
    }

    private FileIOError readOldFileFragments()
    {
        Stack<String> tokenStack = new Stack<>();
        boolean insideFragment = false, inputCorrupted = false;
        int corruptedLineNumber = 0;

        for(int i = 0; i < inputLines_.size(); ++i)
        {
            closingMatcher_.reset(inputLines_.get(i));

            if (closingMatcher_.matches())
            {
                if (insideFragment)
                {
                    String token = tokenize(inputLines_.get(i), closingMatcher_);

                    if (tokenStack.empty() || !tokenStack.pop().equals(token))
                    {
                        inputCorrupted = true;
                        corruptedLineNumber = i + 1;
                        break;
                    }

                    if (!fragments_.containsKey(token))
                    {
                        fragments_.put(token, new ArrayList<>());
                        fragments_.get(token).add("");
                    }

                    insideFragment = false;
                }
                else
                {
                    inputCorrupted = true;
                    corruptedLineNumber = i + 1;
                    break;
                }
            }

            if(insideFragment)
            {
                if (!fragments_.containsKey(tokenStack.peek()))
                    fragments_.put(tokenStack.peek(), new ArrayList<>());

                fragments_.get(tokenStack.peek()).add(inputLines_.get(i));
            }

            openingMatcher_.reset(inputLines_.get(i));

            if (openingMatcher_.matches())
            {
                if (!insideFragment)
                {
                    String token = tokenize(inputLines_.get(i), openingMatcher_);

                    tokenStack.add(token);

                    insideFragment = true;
                }
            }
        }

        if (inputCorrupted)
        {
            return new FileIOError(toUpdateFilePath_, corruptedLineNumber, "Token mismatch");
        }

        if (!tokenStack.empty())
        {
            StringBuilder errorMsg = new StringBuilder("Token mismatch: " + tokenStack.pop());
            while (!tokenStack.empty())
                errorMsg.append(", ").append(tokenStack.pop());
            return new FileIOError(toUpdateFilePath_, 0, errorMsg.toString());
        }

        return null;
    }

    private FileIOError writeFileFragments()
    {
        Stack<String> tokenStack = new Stack<>();
        boolean insideFragment = false, inputCorrupted = false;
        int corruptedLineNumber = 0;

        for (int i = 0; i < outputLines_.size(); ++i)
        {
            closingMatcher_.reset(outputLines_.get(i));

            if (insideFragment)
            {
                int linesRemoved = 0;

                while (!closingMatcher_.matches())
                {
                    outputLines_.remove(i);
                    closingMatcher_.reset(outputLines_.get(i));
                    linesRemoved++;
                }

                String token = tokenize(outputLines_.get(i), closingMatcher_);

                if (tokenStack.empty() || !tokenStack.pop().equals(token))
                {
                    inputCorrupted = true;
                    corruptedLineNumber = i + 1 + linesRemoved;
                    break;
                }

                for (String fragmentLine : fragments_.get(token))
                {
                    outputLines_.add(i, fragmentLine);
                    ++i;
                }

                insideFragment = false;
            }
            else
            {
                if (closingMatcher_.matches())
                {
                    inputCorrupted = true;
                    corruptedLineNumber = i+ 1;
                    break;
                }
            }

            openingMatcher_.reset(outputLines_.get(i));

            if (openingMatcher_.matches())
            {
                String token = tokenize(outputLines_.get(i), openingMatcher_);

                tokenStack.add(token);

                insideFragment = true;
            }
        }

        if (inputCorrupted)
        {
            return new FileIOError(mergeNDeleteFilePath_, corruptedLineNumber, "Token mismatch");
        }

        if (!tokenStack.empty())
        {
            StringBuilder errorMsg = new StringBuilder("Token mismatch: " + tokenStack.pop());
            while (!tokenStack.empty())
                errorMsg.append(", ").append(tokenStack.pop());
            return new FileIOError(mergeNDeleteFilePath_, 0, errorMsg.toString());
        }

        return null;
    }

    private String tokenize(String tokenLine, Matcher matcher)
    {
        String clause;

        if(matcher == openingMatcher_)
            clause = openingClause_;
        else
            clause = closingClause_;

        String token = tokenLine.replaceAll(clause, "");
        token = token.replaceAll("[ \\n\\t\\*/]", "");

        return token;
    }

    public static void backupFiles(Path toUpdateFilePath, Path mergeNDeleteFilePath) throws IOException
    {
        Path backupDirPath = Paths.get(System.getProperty("user.dir"), "C_to_CPP_backups");
        File backupDir = backupDirPath.toFile();

        if (!backupDir.exists() || !backupDir.isDirectory())
        {
            //TO DO: notify user in case of failure
            if (!backupDir.mkdir())
                return;
        }

        Path backupToUpdatePath = backupDirPath.resolve(toUpdateFilePath.getFileName());
        Path backupMergeNDeletePath = backupDirPath.resolve(mergeNDeleteFilePath.getFileName());

        Files.copy(toUpdateFilePath, backupToUpdatePath, REPLACE_EXISTING);
        Files.copy(mergeNDeleteFilePath, backupMergeNDeletePath, REPLACE_EXISTING);
    }
}
