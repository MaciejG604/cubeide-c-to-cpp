package FileMerger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;

public class InFileInstanceReplacer
{
    private Path filePath_;
    private String oldClause_, newClause_;

    public InFileInstanceReplacer( Path inputPath, String oldClause, String newClause)
    {
        filePath_ = inputPath;
        oldClause_ = oldClause;
        newClause_ = newClause;
    }

    public void replace() throws IOException
    {
        ArrayList<String> fileLines = new ArrayList<>();
        fileLines.addAll(Files.readAllLines(filePath_));

        for(int i = 0; i < fileLines.size(); ++i)
        {
            fileLines.set(i, fileLines.get(i).replaceAll(oldClause_, newClause_));
        }

        Files.write(filePath_, fileLines);
    }
}
